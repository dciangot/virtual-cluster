## virtual-cluster

### Overview

Connect clusters together under a single "master cluster" - across
regions and clouds. Pods scheduled onto a virtual kubelet node are forwarded
to the target cluster, and are processed by the kube-scheduler running in
that cluster to choose the actual node to run on.

![](../img/virtual-multicluster.png)

The capacity of the virtual kublet (CPUs, memory, pods) is the sum of the capacities
of healthy nodes in the target cluster.

Alternatively, the capacity can be manually set using labels on the node itself,
which is necessary for autoscaling clusters where the current capacity is not
always equal to the maximum capacity.

### Restrictions

#### Pod Dependencies

Pods should be designed to run with no dependencies in the cluster.
Persistent volumes, services, etc. will not be accessible in the external
clusters.

The exception to this rule is secrets. When a pod is forwarded to an external
cluster the secrets that it depends on will also be created in that cluster.
This is necessary to be able to give pods credentials for pulling/pushing
input and output to a shared data store. The exact mechanism for doing this
may change, but it is definitely required in some way.

Pods are stripped down to a minimal representation in [strip.go](../strip.go).

#### Service Accounts

Currently it is assumed that pods will run with the default service account of whichever
namespace they are running in. The serviceAccountName field of the pod is stripped from the
pod definition, and any secret with a name starting with "default-token" is ignored as this will
be re-populated with the serviceaccount token of the namespace in the target cluster.

#### Pod Environment

Pods can have information about themselves exposed to them in their
environment by using
[fieldRefs](https://kubernetes.io/docs/tasks/inject-data-application/environment-variable-expose-pod-information/).

These references are evaluated when the pod is pushed to the (virtual) kubelet
in the master cluster, so node name, pod IP, and any other
fields which change when the pod is submitted to the target cluster
will no longer be accurate.

Because the fieldRefs have been substituted with the raw value, so it is not possible
to tell that the value originally came from a fieldRef in order to reset it.

This could possibly be remedied by getting the pod manifest from the master
cluster API to check what was originally submitted, but this is not a priority.

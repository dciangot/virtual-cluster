package main

import (
	"context"
	goerrors "errors"
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	"github.com/virtual-kubelet/node-cli/provider"
	"github.com/virtual-kubelet/virtual-kubelet/errdefs"
	"github.com/virtual-kubelet/virtual-kubelet/log"
	"github.com/virtual-kubelet/virtual-kubelet/node/api"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	//"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/tools/remotecommand"
	clientwatch "k8s.io/client-go/tools/watch"
)

type VirtualCluster struct {
	master   *kubernetes.Clientset
	client   *kubernetes.Clientset
	config   *rest.Config
	podIP    string
	nodeName string
	version  string

	// Keep track of the capacity set with labels, so we can know when it has changed
	labelledCapacity *clusterCapacity
}

// NewVirtualCluster reads a kubeconfig file and sets up a client to interact
// with another cluster.
func NewVirtualCluster(cfg provider.InitConfig) (*VirtualCluster, error) {
	config, err := clientcmd.BuildConfigFromFlags("", cfg.ConfigPath)
	if err != nil {
		return nil, fmt.Errorf("could not read config file for cluster: %v", err)
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, fmt.Errorf("could not build clientset for cluster: %v", err)
	}

	masterConfig, err := rest.InClusterConfig()
	if err != nil {
		return nil, fmt.Errorf("could not create in cluster config: %v", err)
	}

	master, err := kubernetes.NewForConfig(masterConfig)
	if err != nil {
		return nil, fmt.Errorf("could not create client for master cluster: %v", err)
	}

	discoveryClient, err := discovery.NewDiscoveryClientForConfig(config)
	if err != nil {
		return nil, fmt.Errorf("could not create discovery client: %v", err)
	}

	serverVersion, err := discoveryClient.ServerVersion()
	if err != nil {
		return nil, fmt.Errorf("could not get target cluster server version: %v", err)
	}

	// The IP of the other cluster should be reported as the IP of the virtual cluster pod.
	podIP := os.Getenv("POD_IP")
	if len(podIP) == 0 {
		return nil, fmt.Errorf("env var POD_IP was not set")
	}

	virtualCluster := &VirtualCluster{
		master:   master,
		client:   clientset,
		config:   config,
		podIP:    podIP,
		nodeName: cfg.NodeName,
		version:  serverVersion.GitVersion,

		labelledCapacity: &clusterCapacity{},
	}

	return virtualCluster, nil
}

// CreatePod takes a Kubernetes Pod and deploys it within the provider.
func (v *VirtualCluster) CreatePod(ctx context.Context, pod *corev1.Pod) error {
	basicPod := stripPod(pod)

	basicPod.ObjectMeta.Labels = make(map[string]string)
	basicPod.ObjectMeta.Labels["virtual-cluster-pod"] = "true"

	if _, err := v.client.CoreV1().Namespaces().Get(pod.Namespace, metav1.GetOptions{}); errors.IsNotFound(err) {
		log.G(ctx).Infof("Namespace %s does not exist for pod %s, creating it", pod.Namespace, pod.Name)
		ns := &corev1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name: pod.Namespace,
			},
		}
		v.client.CoreV1().Namespaces().Create(ns)
	}

	// TODO: Instead of creating secrets in the target cluster, could read the secrets in the master
	// and use an init container to write them into the pod?
	secretNames := getSecrets(pod)
	for _, secretName := range secretNames {
		_, err := v.client.CoreV1().Secrets(pod.Namespace).Get(secretName, metav1.GetOptions{})
		if err == nil {
			continue
		}
		if errors.IsNotFound(err) {
			secret, err := v.master.CoreV1().Secrets(pod.Namespace).Get(secretName, metav1.GetOptions{})
			if err != nil {
				continue
			}
			secret.ObjectMeta.UID = ""
			secret.ObjectMeta.ResourceVersion = ""
			//secret.ObjectMeta.CreationTimestamp = time.Time{}
			secret.ObjectMeta.SelfLink = ""
			_, err = v.client.CoreV1().Secrets(pod.Namespace).Create(secret)
			if err != nil {
				log.G(ctx).WithFields(log.Fields{"name": secret.Name}).WithError(err).Error("Failed to create secret")
			}
			continue
		}
		return fmt.Errorf("could not check secret %s in external cluster: %v", secretName, err)
	}

	_, err := v.client.CoreV1().Pods(pod.Namespace).Create(basicPod)
	if err != nil {
		return fmt.Errorf("could not create pod: %v", err)
	}
	return nil
}

// UpdatePod takes a Kubernetes Pod and updates it within the provider.
func (v *VirtualCluster) UpdatePod(ctx context.Context, pod *corev1.Pod) error {
	currentPod, err := v.GetPod(ctx, pod.Namespace, pod.Name)
	if err != nil {
		return fmt.Errorf("could not get current pod")
	}
	updated := updatePod(currentPod, pod)
	_, err = v.client.CoreV1().Pods(pod.Namespace).Update(updated)
	if err != nil {
		return fmt.Errorf("could not update pod: %v", err)
	}
	return nil
}

// DeletePod takes a Kubernetes Pod and deletes it from the provider.
func (v *VirtualCluster) DeletePod(ctx context.Context, pod *corev1.Pod) error {
	opts := &metav1.DeleteOptions{
		GracePeriodSeconds: new(int64), // 0
	}
	err := v.client.CoreV1().Pods(pod.Namespace).Delete(pod.Name, opts)
	if err != nil {
		if errors.IsNotFound(err) {
			log.G(ctx).Infof("Tried to delete pod %s/%s, but it did not exist in the cluster", pod.Namespace, pod.Name)
			return nil
		}
		return fmt.Errorf("could not delete pod: %v", err)
	}
	return nil
}

// GetPod retrieves a pod by name from the provider (can be cached).
// The Pod returned is expected to be immutable, and may be accessed
// concurrently outside of the calling goroutine. Therefore it is recommended
// to return a version after DeepCopy.
func (v *VirtualCluster) GetPod(ctx context.Context, namespace string, name string) (*corev1.Pod, error) {
	pod, err := v.client.CoreV1().Pods(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		if errors.IsNotFound(err) {
			return nil, errdefs.AsNotFound(err)
		}
		return nil, fmt.Errorf("could not get pod %s/%s: %v", namespace, name, err)
	}
	pod.Status.PodIP = ""
	return pod.DeepCopy(), nil
}

// GetPodStatus retrieves the status of a pod by name from the provider.
// The PodStatus returned is expected to be immutable, and may be accessed
// concurrently outside of the calling goroutine. Therefore it is recommended
// to return a version after DeepCopy.
func (v *VirtualCluster) GetPodStatus(ctx context.Context, namespace string, name string) (*corev1.PodStatus, error) {
	pod, err := v.client.CoreV1().Pods(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return nil, fmt.Errorf("could not get pod %s/%s: %v", namespace, name, err)
	}
	pod.Status.PodIP = ""
	return pod.Status.DeepCopy(), nil
}

// GetPods retrieves a list of all pods running on the provider (can be cached).
// The Pods returned are expected to be immutable, and may be accessed
// concurrently outside of the calling goroutine. Therefore it is recommended
// to return a version after DeepCopy.
func (v *VirtualCluster) GetPods(_ context.Context) ([]*corev1.Pod, error) {
	pods, err := v.client.CoreV1().Pods("").List(metav1.ListOptions{LabelSelector: "virtual-cluster-pod=true"})
	if err != nil {
		return nil, fmt.Errorf("could not list pods: %v", err)
	}

	podRefs := []*corev1.Pod{}
	for _, p := range pods.Items {
		podRefs = append(podRefs, p.DeepCopy())
	}

	return podRefs, nil
}

// GetContainerLogs retrieves the logs of a container by name from the provider.
func (v *VirtualCluster) GetContainerLogs(ctx context.Context, namespace string, podName string, containerName string, opts api.ContainerLogOpts) (io.ReadCloser, error) {
	options := &corev1.PodLogOptions{
		Container: containerName,
	}
	logs := v.client.CoreV1().Pods(namespace).GetLogs(podName, options)
	stream, err := logs.Stream()
	if err != nil {
		return nil, fmt.Errorf("could not get stream from logs request: %v", err)
	}
	return stream, nil
}

// RunInContainer executes a command in a container in the pod, copying data
// between in/out/err and the container's stdin/stdout/stderr.
func (v *VirtualCluster) RunInContainer(ctx context.Context, namespace string, podName string, containerName string, cmd []string, attach api.AttachIO) error {

	req := v.client.CoreV1().RESTClient().
		Post().
		Namespace(namespace).
		Resource("pods").
		Name(podName).
		SubResource("exec").
		VersionedParams(&corev1.PodExecOptions{
			Container: containerName,
			Command:   cmd,
			Stdin:     true,
			Stdout:    true,
			Stderr:    true,
			TTY:       true,
		}, scheme.ParameterCodec)

	exec, err := remotecommand.NewSPDYExecutor(v.config, "POST", req.URL())
	if err != nil {
		return fmt.Errorf("could not make remote command: %v", err)
	}

	err = exec.Stream(remotecommand.StreamOptions{
		Stdin:  attach.Stdin(),
		Stdout: attach.Stdout(),
		Stderr: attach.Stderr(),
		Tty:    attach.TTY(),
	})
	if err != nil {
		return fmt.Errorf("streaming error: %v", err)
	}

	return nil
}

// ConfigureNode enables a provider to configure the node object that
// will be used for Kubernetes.
func (v *VirtualCluster) ConfigureNode(_ context.Context, node *corev1.Node) {
	nodes, err := v.client.CoreV1().Nodes().List(metav1.ListOptions{})
	if err != nil {
		return
	}

	capacity := &clusterCapacity{}

	for _, n := range nodes.Items {
		if _, found := n.Labels["node-role.kubernetes.io/master"]; found {
			continue
		}

		for _, t := range n.Spec.Taints {
			if t.Key == "node.kubernetes.io/unschedulable" {
				continue
			}
		}

		if n.Spec.Unschedulable {
			continue
		}

		(&capacity.maxCPU).Add(n.Status.Capacity[corev1.ResourceCPU])
		(&capacity.maxMem).Add(n.Status.Capacity[corev1.ResourceMemory])
		(&capacity.maxPods).Add(n.Status.Capacity[corev1.ResourcePods])
	}

	setCapacityOnNode(capacity, node)

	nodeObj, err := v.master.CoreV1().Nodes().Get(node.Name, metav1.GetOptions{})
	if err == nil {
		labelCapacity, err := capacityFromNode(nodeObj)
		if err == nil {
			// Only updates the non-zero resource quantities, so it's ok to call this always
			setCapacityOnNode(labelCapacity, node)
			v.labelledCapacity = labelCapacity
		}
	}

	node.Status.NodeInfo.KubeletVersion = v.version
	node.Status.Addresses = []corev1.NodeAddress{{Type: corev1.NodeInternalIP, Address: v.podIP}}
	node.Status.Conditions = perfectNodeConditions()

	return
}

// implement node.NodeProvider
func (v *VirtualCluster) Ping(ctx context.Context) error {
	cs, err := v.client.CoreV1().ComponentStatuses().List(metav1.ListOptions{})
	if err != nil {
		log.G(ctx).WithError(err).Error("Failed ping")
		return fmt.Errorf("could not list component statuses: %v", err)
	}

	faults := []string{}

	for _, component := range cs.Items {
		for _, condition := range component.Conditions {
			if condition.Type == corev1.ComponentHealthy {
				if condition.Status != corev1.ConditionTrue {
					faults = append(faults, fmt.Sprintf("%s: %s", component.Name, condition.Message))
				}
			}
		}
	}

	if len(faults) > 0 {
		log.G(ctx).WithFields(log.Fields{"faults": strings.Join(faults, ", ")}).Error("Failed ping")
		return goerrors.New(strings.Join(faults, ", "))
	}

	return nil
}

func (v *VirtualCluster) NotifyNodeStatus(ctx context.Context, cb func(*corev1.Node)) {
	go v.watchCapacity(ctx, cb)
}

func (v *VirtualCluster) watchCapacity(ctx context.Context, cb func(*corev1.Node)) {
	// Need to get the initial resourceVersion to be able to start the watch.
	// It doesn't exist until virtual kubelet has created it, so have to wait a moment
	// before trying to get it.
	time.Sleep(5 * time.Second)

	initialNode, err := v.master.CoreV1().Nodes().Get(v.nodeName, metav1.GetOptions{})
	if err != nil {
		log.G(ctx).WithError(err).Fatal("Could not get initial node in capacity watcher")
	}

	selectNode := fields.OneTermEqualSelector("metadata.name", v.nodeName)
	watcher := cache.NewListWatchFromClient(v.master.CoreV1().RESTClient(), "nodes", "", selectNode)
	retryWatcher, err := clientwatch.NewRetryWatcher(initialNode.ResourceVersion, watcher)
	if err != nil {
		log.G(ctx).WithError(err).Fatal("Could not get create node watcher")
	}

	for event := range retryWatcher.ResultChan() {
		if event.Type != watch.Modified {
			continue
		}
		node, ok := event.Object.(*corev1.Node)
		if !ok {
			log.G(ctx).Warn("Node watcher received non-node object")
			continue
		}

		capacity, err := capacityFromNode(node)
		if err != nil {
			log.G(ctx).WithError(err).Error("Could not get node capacity")
			continue
		}

		// The notify callback only needs to be called if the capacity labels have changed,
		// so compare to the saved state
		if !capacity.Equals(v.labelledCapacity) {
			log.G(ctx).WithFields(log.Fields{
				"old": v.labelledCapacity,
				"new": capacity,
			}).Info("Pod labelled capacity changed")
			v.labelledCapacity = capacity
			setCapacityOnNode(capacity, node)
			cb(node)
		}
	}

	log.G(ctx).Error("Watch capacity ended (should never happen)")
}

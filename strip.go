package main

import (
	"strings"

	"github.com/virtual-kubelet/virtual-kubelet/log"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Functions for stripping fields from pods/containers that can not be
// forwarded to another cluster.

// stripContainers selects the basic components of a list of containers.
func stripContainers(containers []corev1.Container) []corev1.Container {
	var stripped []corev1.Container

	for _, c := range containers {
		volMounts := []corev1.VolumeMount{}
		for _, v := range c.VolumeMounts {
			if strings.HasPrefix(v.Name, "default-token") {
				continue
			}
			volMounts = append(volMounts, v)
		}

		s := corev1.Container{
			Name:            c.Name,
			Image:           c.Image,
			Command:         c.Command,
			Args:            c.Args,
			Ports:           c.Ports,
			Resources:       c.Resources,
			ImagePullPolicy: c.ImagePullPolicy,
			Env:             c.Env,
			VolumeMounts:    volMounts,
		}
		stripped = append(stripped, s)
	}

	return stripped
}

// stripPod creates a view of the pod with only the minimum required fields.
//
// Don't want to copy everything from the Pod that kubernetes creates,
// e.g the volume for the serviceaccount secret has to be different
// in the target cluster.
//
// This can be expanded for more fields or even changed to blacklist
// only certain things that are cluster specific.
func stripPod(pod *corev1.Pod) *corev1.Pod {
	vols := []corev1.Volume{}
	for _, v := range pod.Spec.Volumes {
		if strings.HasPrefix(v.Name, "default-token") {
			continue
		}
		vols = append(vols, v)
	}

	return &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:        pod.Name,
			Namespace:   pod.Namespace,
			Annotations: pod.Annotations,
		},
		Spec: corev1.PodSpec{
			Containers:     stripContainers(pod.Spec.Containers),
			InitContainers: stripContainers(pod.Spec.InitContainers),
			RestartPolicy:  pod.Spec.RestartPolicy,
			Volumes:        vols,
		},
	}
}

// getSecrets filters the volumes of a pod to get only the secret volumes,
// excluding the serviceaccount token secret which is automatically added by kuberentes.
func getSecrets(pod *corev1.Pod) []string {
	secretNames := []string{}
	for _, v := range pod.Spec.Volumes {
		if v.Secret == nil {
			continue
		}
		if strings.HasPrefix(v.Name, "default-token") {
			continue
		}
		log.L.Infof("pod %s depends on secret %s", pod.Name, v.Secret.SecretName)
		secretNames = append(secretNames, v.Secret.SecretName)
	}
	return secretNames
}

// updatePod applies only the allowed changes to the original pod object.
//
// This is necessary because updating the entire pod would try to change
// the pod UID, node name, and other fields that should not change.
func updatePod(orig, update *corev1.Pod) *corev1.Pod {
	for i := range orig.Spec.InitContainers {
		orig.Spec.InitContainers[i].Image = update.Spec.InitContainers[i].Image
	}
	for i := range orig.Spec.Containers {
		orig.Spec.Containers[i].Image = update.Spec.Containers[i].Image
	}
	orig.Spec.ActiveDeadlineSeconds = update.Spec.ActiveDeadlineSeconds
	orig.Spec.Tolerations = update.Spec.Tolerations
	return orig
}
